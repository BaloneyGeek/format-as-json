/* Copyright 2019 Boudhayan Gupta <bgupta@kde.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

"use babel";

import { CompositeDisposable } from "atom";

export default {
    subscriptions: null,

    activate(state) {
        this.subscriptions = new CompositeDisposable();
        this.subscriptions.add(atom.commands.add("atom-workspace", {
            "format-as-json:to-json": () => this.toJson()
        }));
    },

    deactivate() {
        this.subscriptions.dispose();
    },

    toJson() {
        let toJsonDoc = (oJs) => {
            return JSON.stringify(eval("(" + oJs + ")"));
        };

        let oEditor;
        if (oEditor = atom.workspace.getActiveTextEditor()) {
            let sSelection = oEditor.getSelectedText();
            sSelection ? oEditor.insertText(toJsonDoc(sSelection)) :
                         oEditor.setText(toJsonDoc(oEditor.getText()));
        }
    }
};
