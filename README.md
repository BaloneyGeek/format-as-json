# Format As JSON

This Atom plugin formats any valid JavaScript Object to a JSON Document

This package does not live on GitHub as a matter of principle. To install this plugin, simply clone the repo to `~/.atom/packages` - or to wherever your Atom packages live.

**WARNING**: This plugin is not made with security in mind. In particular, it `eval()`s your selection or *the entire contents of the current text editor, if there is no selection*. EVEN THE SLIGHTEST MISTAKE MIGHT RESULT IN DATA LOSS OR DISCLOSURE OF ALL DATA IN YOUR LOCAL COMPUTER AS WELL AS ALL DATA IN EVERY DEVICE ON NETWORKS YOU ARE CURRENTLY CONNECTED TO AS WELL AS NETWORKS YOU WILL CONNECT TO IN THE FUTURE. Use this plugin entirely at your own risk.
